const Database = require('better-sqlite3');
const schedule = require('node-schedule');

// Execute the script at a randomized time between midnight and 5AM
schedule.scheduleJob(
	{
		minute: Math.floor(Math.random() * 60),
		hour: Math.floor(Math.random() * 5),
		tz: Intl.DateTimeFormat().resolvedOptions().timeZone,
	},
	() => {
		console.log('Cleaning up unused projects...');

		const sixMonthsAgo = new Date();
		sixMonthsAgo.setMonth(sixMonthsAgo.getMonth() - 6);

		const db = new Database('splitt.db');

		const deletedProjectsCount = db
			.prepare(`
				DELETE FROM project
				WHERE last_used < '${sixMonthsAgo.toISOString()}'
			`)
			.run()
			.changes;

		db.close();

		console.log(
			deletedProjectsCount === 1
				? '1 project has been deleted.'
				: `${deletedProjectsCount} projects have been deleted.`
		);
	},
);
