PRAGMA foreign_keys = ON;

DROP TABLE IF EXISTS project;
DROP TABLE IF EXISTS expense;

CREATE TABLE project (
	id TEXT PRIMARY KEY NOT NULL,
	title TEXT NOT NULL,
	description TEXT,
	currency TEXT NOT NULL,
	participants TEXT NOT NULL,
	last_used TEXT NOT NULL
);

CREATE TABLE expense (
	id TEXT PRIMARY KEY NOT NULL,
	project_id TEXT NOT NULL,
	title TEXT NOT NULL,
	amount REAL NOT NULL,
	payer TEXT NOT NULL,
	participants TEXT NOT NULL,
	date TEXT NOT NULL,
	FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE
);

INSERT INTO project (id, title, description, currency, participants, last_used)
VALUES (
	'09pguo49mx7v',
	'Some project',
	'Some very cool project descripton',
	'EUR',
	'Alice,Bob,Charles',
	'2100-12-12T23:59:59.999Z'
);

INSERT INTO expense (id, project_id, title, amount, payer, participants, date)
VALUES (
	'v4so0sj1nwu4',
	'09pguo49mx7v',
	'AirBnB',
	300,
	'Alice',
	'Alice,Bob,Charles',
	'2022-04-09T19:45:01.000Z'
), (
	'a14c8kpx080l',
	'09pguo49mx7v',
	'Airplane tickets',
	400,
	'Bob',
	'Alice,Bob,Charles',
	'2022-04-10T20:45:01.000Z'
), (
	'xrjnl9g3fy9s',
	'09pguo49mx7v',
	'Hats',
	30,
	'Charles',
	'Bob,Charles',
	'2022-04-11T21:45:01.000Z'
);
