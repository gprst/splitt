PRAGMA journal_mode = WAL;
PRAGMA foreign_keys = ON;
PRAGMA auto_vacuum = FULL;

CREATE TABLE IF NOT EXISTS project (
	id TEXT PRIMARY KEY NOT NULL,
	title TEXT NOT NULL,
	description TEXT,
	currency TEXT NOT NULL,
	participants TEXT NOT NULL,
	last_used TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS expense (
	id TEXT PRIMARY KEY NOT NULL,
	project_id TEXT NOT NULL,
	title TEXT NOT NULL,
	amount REAL NOT NULL,
	payer TEXT NOT NULL,
	participants TEXT NOT NULL,
	date TEXT NOT NULL,
	FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE
);