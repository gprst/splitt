import { ValidationPipe, VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { AppModule } from './app.module';

if (process.env.NODE_ENV === 'production') {
	import('../scripts/schedule-projects-cleaning');
}

async function bootstrap() {
	const app = await NestFactory.create<NestFastifyApplication>(
		AppModule,
		new FastifyAdapter({ logger: true }),
	);
	app.setGlobalPrefix('/api');
	app.enableCors({
		exposedHeaders: ['location'],
		origin: [
			'http://127.0.0.1:5173', // Web app development URL
			'http://127.0.0.1:4173', // Web app production URL
			'http://localhost', // Android app URL
		],
	});
	app.enableVersioning({
		type: VersioningType.URI,
		defaultVersion: '1',
	});
	app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
	await app.listen(5174);
}
bootstrap();
