import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { TypeOrmModule } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { ProjectsModule } from './projects/projects.module';

const isProduction = process.env.NODE_ENV === 'production';

@Module({
	imports: [
		ProjectsModule,
		EventEmitterModule.forRoot(),
		ThrottlerModule.forRoot({
			ttl: 60,
			limit: 100,
		}),
		TypeOrmModule.forRoot({
			type: 'better-sqlite3',
			database: isProduction ? 'splitt.db' : ':memory:',
			autoLoadEntities: true,
			prepareDatabase(db) {
				const initScript = readFileSync(
					isProduction
						? `${process.cwd()}/scripts/init-db/prod.sql`
						: `${process.cwd()}/scripts/init-db/dev.sql`,
				).toString();

				db.exec(initScript);
			},
		}),
	],
	providers: [
		{
			provide: APP_GUARD,
			useClass: ThrottlerGuard,
		},
	],
})
export class AppModule {}
