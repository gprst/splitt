import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { fromEvent } from 'rxjs';

@Injectable()
export class EventsService {
	private readonly eventEmitter: EventEmitter2;

	constructor() {
		this.eventEmitter = new EventEmitter2();
	}

	subscribe(eventName: string) {
		return fromEvent(this.eventEmitter, eventName);
	}

	emit(eventName: string, data: unknown) {
		this.eventEmitter.emit(eventName, { data });
	}
}