import { uid } from 'uid/secure';

export default function newId(): string {
	return uid(12);
}
