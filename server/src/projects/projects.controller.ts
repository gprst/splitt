import {
	Body,
	Controller,
	Get,
	MessageEvent,
	Param,
	Post,
	Put,
	Req,
	Res,
	Sse,
} from '@nestjs/common';
import { FastifyReply, FastifyRequest } from 'fastify';
import { Observable } from 'rxjs';
import { EventsService } from 'src/events.service';
import CreateProjectDto from './dto/create-project.dto';
import GetProjectDto from './dto/get-project.dto';
import UpdateProjectDto from './dto/update-project.dto';
import { getProjectUpdateEventName, ProjectsService } from './projects.service';

@Controller('projects')
export class ProjectsController {
	constructor(
		private projectsService: ProjectsService,
		private eventsService: EventsService,
	) {}

	@Post()
	create(
		@Body() newProject: CreateProjectDto,
		@Req() req: FastifyRequest,
		@Res({ passthrough: true }) res: FastifyReply,
	): void {
		const newId = this.projectsService.create(newProject);

		res.header('location', `${req.routerPath}/${newId}`);
	}

	@Get(':id')
	findOne(@Param('id') id: string): Promise<GetProjectDto> {
		return this.projectsService.findOne(id);
	}

	@Put(':id')
	update(@Param('id') id: string, @Body() updatedProject: UpdateProjectDto) {
		return this.projectsService.update({ ...updatedProject, id });
	}

	@Sse(':id/sse')
	projectUpdateSse(@Param('id') id: string): Observable<Promise<MessageEvent>> {
		const eventName = getProjectUpdateEventName(id);

		return this.eventsService.subscribe(eventName) as Observable<Promise<MessageEvent>>;
	}
}
