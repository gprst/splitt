import { Injectable, MessageEvent, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventsService } from 'src/events.service';
import { Repository } from 'typeorm';
import newId from 'utils/new-id';
import { Project as ProjectEntity } from './project.entity';
import Project from './types/project.type';

export function getProjectUpdateEventName(projectId: string): string {
	return `projects/${projectId}/update`;
}

@Injectable()
export class ProjectsService {
	constructor(
		@InjectRepository(ProjectEntity) private projectsRepository: Repository<ProjectEntity>,
		private eventsService: EventsService,
	) {}

	create(project: Omit<Project, 'id'>): string {
		const id = newId();

		this.projectsRepository.insert({
			...project,
			id,
			lastUsed: new Date().toISOString(),
		});

		return id;
	}

	async findOne(id: string): Promise<Project> {
		const project = await this.projectsRepository.findOneBy({ id });

		if (!project) {
			throw new NotFoundException(`Project with ID ${id} not found`);
		}

		this.update(project);

		const { lastUsed, ...projectToReturn } = project;

		return projectToReturn;
	}

	async update(project: Project) {
		const projectExists = Boolean(await this.projectsRepository.findOneBy({ id: project.id }));

		if (!projectExists) {
			throw new NotFoundException(`Project with ID ${project.id} not found`);
		}

		const updatedProject: ProjectEntity = {
			...project,
			lastUsed: new Date().toISOString(),
		}

		await this.projectsRepository.save(updatedProject);

		this.eventsService.emit(getProjectUpdateEventName(project.id), updatedProject);
	}
}
