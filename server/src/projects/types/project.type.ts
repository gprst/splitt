import { CurrencyCode } from './currency-code.type';

type Project = {
	id: string;
	title: string;
	description?: string;
	currency: CurrencyCode;
	participants: string[];
};

export default Project;
