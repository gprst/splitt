export const currencyCodes = ['EUR', 'USD'] as const;

export type CurrencyCode = typeof currencyCodes[number];
