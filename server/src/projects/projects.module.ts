import { Module } from '@nestjs/common';
import { ProjectsController } from './projects.controller';
import { ProjectsService } from './projects.service';
import { ExpensesController } from './expenses/expenses.controller';
import { ExpensesService } from './expenses/expenses.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './project.entity';
import { Expense } from './expenses/expense.entity';
import { EventsService } from 'src/events.service';

@Module({
	imports: [TypeOrmModule.forFeature([Project, Expense])],
	controllers: [ProjectsController, ExpensesController],
	providers: [ProjectsService, ExpensesService, EventsService],
})
export class ProjectsModule {}
