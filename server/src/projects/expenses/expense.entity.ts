import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Expense {
	@PrimaryColumn()
	id: string;

	@Column({ name: 'project_id' })
	projectId: string;

	@Column()
	title: string;

	@Column('real')
	amount: number;

	@Column()
	payer: string;

	@Column('simple-array')
	participants: string[];

	@Column()
	date: string;
}
