import { ArrayMinSize, IsDateString, IsNotEmpty, IsPositive } from 'class-validator';

export default class ExpenseDto {
	@IsNotEmpty()
	title: string;

	@IsPositive()
	amount: number;

	@IsNotEmpty()
	payer: string;

	@ArrayMinSize(1)
	participants: string[];

	@IsDateString()
	date: string;
}
