import {
	Body,
	Controller,
	Delete,
	Get,
	MessageEvent,
	Param,
	Post,
	Put,
	Req,
	Res,
	Sse,
} from '@nestjs/common';
import { FastifyReply, FastifyRequest } from 'fastify';
import { Observable } from 'rxjs';
import { EventsService } from 'src/events.service';
import ExpenseDto from './dto/expense.dto';
import { ExpensesService, getProjectExpensesUpdateEventName } from './expenses.service';

@Controller('projects/:projectId/expenses')
export class ExpensesController {
	constructor(
		private expensesService: ExpensesService,
		private eventsService: EventsService,
	) {}

	@Post()
	create(
		@Param('projectId') projectId: string,
		@Body() expense: ExpenseDto,
		@Req() req: FastifyRequest,
		@Res({ passthrough: true }) res: FastifyReply,
	) {
		const id = this.expensesService.create(projectId, expense);

		res.header('location', `${req.routerPath}/${id}`);
	}

	@Get()
	findAll(@Param('projectId') projectId: string) {
		return this.expensesService.findAll(projectId);
	}

	@Put(':expenseId')
	update(
		@Param('projectId') projectId: string,
		@Param('expenseId') expenseId: string,
		@Body() updatedExpense: ExpenseDto,
	) {
		return this.expensesService.update(projectId, {
			id: expenseId,
			...updatedExpense,
		});
	}

	@Delete(':expenseId')
	delete(@Param('projectId') projectId: string, @Param('expenseId') expenseId: string) {
		return this.expensesService.delete(projectId, expenseId);
	}

	@Sse('sse')
	expensesUpdateSse(@Param('projectId') projectId: string): Observable<Promise<MessageEvent>> {
		const eventName = getProjectExpensesUpdateEventName(projectId);

		return this.eventsService.subscribe(eventName) as Observable<Promise<MessageEvent>>;
	}
}
