import { Injectable, MessageEvent, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { fromEvent, Observable } from 'rxjs';
import { EventsService } from 'src/events.service';
import { Repository } from 'typeorm';
import newId from 'utils/new-id';
import { Expense as ExpenseEntity } from './expense.entity';
import Expense from './types/expense.type';

function mapExpenseEntity(entity: ExpenseEntity): Expense {
	const { projectId, ...expense } = entity;
	return expense;
}

export function getProjectExpensesUpdateEventName(projectId: string): string {
	return `projects/${projectId}/expenses/update`;
}

@Injectable()
export class ExpensesService {
	constructor(
		@InjectRepository(ExpenseEntity) private expensesRepository: Repository<ExpenseEntity>,
		private eventsService: EventsService,
	) {}

	private async emitExpenses(projectId: string) {
		const expenses = (
			(await this.expensesRepository.find({
				where: { projectId },
				order: { date: 'DESC' },
			})) || []
		).map(mapExpenseEntity);
		
		this.eventsService.emit(getProjectExpensesUpdateEventName(projectId), expenses);
	}

	create(projectId: string, expense: Omit<Expense, 'id'>): string {
		const id = newId();

		const newExpense: ExpenseEntity = { ...expense, id, projectId };

		this.expensesRepository.insert(newExpense);

		this.emitExpenses(projectId);

		return id;
	}

	async findAll(projectId: string): Promise<Expense[]> {
		const projectExpenses = await this.expensesRepository.find({
			where: { projectId },
			order: { date: 'DESC' },
		});

		return projectExpenses.map(mapExpenseEntity);
	}

	async update(projectId: string, expense: Expense) {
		const expenseExists = Boolean(await this.expensesRepository.findOneBy({ id: expense.id }));
		if (!expenseExists) {
			throw new NotFoundException(`Expense with ID ${expense.id} not found`);
		}

		await this.expensesRepository.save({ ...expense, projectId });

		this.emitExpenses(projectId);
	}

	async delete(projectId: string, expenseId: string) {
		const expenseExists = Boolean(await this.expensesRepository.findOneBy({ id: expenseId }));
		if (!expenseExists) {
			throw new NotFoundException(`Expense with ID ${expenseId} not found`);
		}

		await this.expensesRepository.delete({ projectId, id: expenseId });

		this.emitExpenses(projectId);
	}
}
