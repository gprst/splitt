type Expense = {
	id: string;
	title: string;
	amount: number;
	payer: string;
	participants: string[];
	date: string;
};

export default Expense;
