import { Column, Entity, PrimaryColumn } from 'typeorm';
import { CurrencyCode } from './types/currency-code.type';

@Entity()
export class Project {
	@PrimaryColumn()
	id: string;

	@Column()
	title: string;

	@Column()
	description?: string;

	@Column()
	currency: CurrencyCode;

	@Column('simple-array')
	participants: string[];

	@Column({ name: 'last_used' })
	lastUsed: string;
}
