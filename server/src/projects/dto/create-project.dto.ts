import { ArrayMinSize, ArrayUnique, IsIn, IsNotEmpty, IsOptional } from 'class-validator';
import { CurrencyCode, currencyCodes } from '../types/currency-code.type';

export default class CreateProjectDto {
	@IsNotEmpty()
	title: string;

	@IsOptional()
	description?: string;

	@IsIn(currencyCodes)
	currency: CurrencyCode;

	@ArrayMinSize(1)
	@ArrayUnique()
	participants: string[];
}
