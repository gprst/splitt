import { IsNotEmpty } from 'class-validator';
import CreateProjectDto from './create-project.dto';

export default class GetProjectDto extends CreateProjectDto {
	@IsNotEmpty()
	id: string;
}
