import CreateProjectDto from './create-project.dto';

export default class UpdateProjectDto extends CreateProjectDto {}
