# Splitt backend

## Quickstart

```bash
pnpm i
pnpm start:dev
```

This will start the backend in development mode, with hot-reload, on the `5174` port.

An in-memory SQLite database pre-populated with test data will be used. The sample project ID is: `09pguo49mx7v`.

## Building for production

Note: you must have the Nest CLI installed globally. You can install it with: `pnpm i -g @nest/cli`.

```bash
pnpm i -P
pnpm build
pnpm start:prod
```

This will build the project, and serve it on the `5174` port.

## Endpoints

### POST `/api/v1/projects`

Create a new project.

<details>
  <summary>Example payload</summary>

  ```json
  {
    "title": "Some project",
    "description": "Some very cool project description", // Optional
    "currency": "EUR", // Can be "EUR" or "USD"
    "participants": [
      "Alice",
      "Bob",
      "Charles"
    ]
  }
  ```
</details>

### GET `/api/v1/projects/{projectId}`

Returns project data by ID.

<details>
  <summary>Example response</summary>

  ```json
  {
    "id": "09pguo49mx7v",
    "title": "Some project",
    "description": "Some very cool project description",
    "currency": "EUR",
    "participants": [
      "Alice",
      "Bob",
      "Charles"
    ]
  }
  ```
</details>

### PUT `/api/v1/projects/{projectId}`

Update a project by ID.

<details>
  <summary>Example payload</summary>

  ```json
  {
    "title": "Some updated project",
    "description": "Some very cool updated description",
    "currency": "USD",
    "participants": [
      "Alice",
      "Bob",
      "Charles",
      "Donald"
    ]
  }
  ```
</details>

### POST `/api/v1/projects/{projectId}/expenses

Add an expense to a project.

<details>
  <summary>Example payload</summary>

  ```json
  {
    "title": "AirBnB",
    "amount": 300.40,
    "payer": "Alice",
    "participants": [
      "Alice",
      "Bob",
      "Charles"
    ],
    "date": "2022-04-09T19:45:01.000Z"
  }
  ```
</details>
</details>

### GET `/api/v1/projects/{projectId}/expenses

Get all expenses of a project.

<details>
  <summary>Example response</summary>

  ```json
  [
    {
      "id": "v4so0sj1nwu4",
      "title": "AirBnB",
      "amount": 300.40,
      "payer": "Alice",
      "participants": [
        "Alice",
        "Bob",
        "Charles"
      ],
      "date": "2022-04-09T19:45:01.000Z"
    }
  ]
  ```
</details>

### PUT `/api/v1/projects/{projectId}/expenses/{expenseId}

Update an expense by ID.

<details>
  <summary>Example payload</summary>

  ```json
  {
    "title": "Hotel",
    "amount": 400.30,
    "payer": "Bob",
    "participants": [
      "Alice",
      "Bob",
      "Charles"
    ],
    "date": "2022-04-11T19:45:01.000Z"
  }
  ```
</details>

### DELETE `/api/v1/projects/{projectId}/expenses/{expenseId}

Remove an expense by ID.
