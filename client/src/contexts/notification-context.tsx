import { AlertColor } from '@mui/material';
import { PropsWithChildren, createContext, useContext } from 'react';
import NotificationsWrapper from '~/components/NotificationsWrapper';
import useNotificationState from '~/hooks/use-notification-state';

export type Notification = {
	message: string;
	severity: AlertColor;
};

type NotificationContextData = {
	pushNotification: (notification: Notification) => void;
};

const NotificationContext = createContext<NotificationContextData | undefined>(undefined);

export function NotificationProvider({ children }: PropsWithChildren) {
	const {
		notificationState,
		setNotificationState,
		setNotificationsStack,
	} = useNotificationState();

	return (
		<NotificationContext.Provider
			value={{
				pushNotification: (notification: Notification) => {
					setNotificationsStack((prev) => prev.concat(notification));
				}
			}}
		>
			<NotificationsWrapper
				notificationState={notificationState}
				setNotificationState={setNotificationState}
			/>

			{children}
		</NotificationContext.Provider>
	);
}

export function useNotification() {
	const context = useContext(NotificationContext);

	if (context === undefined) {
		throw new Error('useNotification must be used within a NotificationContextProvider');
	}

	return context;
}
