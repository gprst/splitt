import { ComponentProps, PropsWithChildren } from 'react';
import AppBar from './components/AppBar';

type Props = {
	appBar?: ComponentProps<typeof AppBar>;
};

function Layout({
	children,
	appBar: {
		projectTitle,
		rightSide,
	} = {},
}: PropsWithChildren<Props>) {
	return (
		<>
			<AppBar projectTitle={projectTitle} rightSide={rightSide} />
			{children}
		</>
	);
}

export default Layout;
