import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import HttpBackend from 'i18next-http-backend';
import { initReactI18next } from 'react-i18next';

i18n
	.use(initReactI18next)
	.use(LanguageDetector)
	.use(HttpBackend)
	.init({
		backend: {
			loadPath: '/locales/{{lng}}/{{ns}}.json',
		},
		fallbackLng: 'en',
		interpolation: {
			escapeValue: false,
		},
		load: 'languageOnly',
		ns: ['common', 'landing', 'project'],
	});

export default i18n;
