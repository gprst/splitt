import { CurrencyCode } from './currencies';

export type Expense = {
	id: string;
	title: string;
	amount: number;
	payer: string;
	participants: string[];
	date: string;
};

export type Project = {
	id: string;
	title: string;
	description?: string;
	currency: CurrencyCode;
	participants: string[];
};

export type ProjectData = Pick<Project, 'id' | 'title' | 'description'> & {
	username: string;
};

export type Settlement = {
	to: string;
	from: string;
	amount: number;
};

export type ApiErrorResponse = {
	statusCode: number;
	message: string | string[];
	error: string;
};

export type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;
