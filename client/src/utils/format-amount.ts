import { CurrencyCode } from '~/currencies';

export default function formatAmount(amount: number, currency?: CurrencyCode): string {
	const formatter = Intl.NumberFormat('en-US', {
		currency,
		style: currency ? 'currency' : 'decimal',
	});

	return formatter.format(amount);
}
