export default function capitalizeFirstLetter(str: string): string {
	return str.charAt(0) + str.slice(1);
}
