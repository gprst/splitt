import { App, URLOpenListenerEvent } from '@capacitor/app';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function useAppUrlOpenListener() {
	const navigate = useNavigate();


	useEffect(() => {
		const urlPattern = /https?:\/\/\w+\.\w+(\/.*)/;

		App.addListener('appUrlOpen', (event: URLOpenListenerEvent) => {
			const slug = urlPattern.exec(event.url)?.[1];

			if (slug) {
				navigate(slug);
			}
		});
	}, [navigate]);
}

export default useAppUrlOpenListener;
