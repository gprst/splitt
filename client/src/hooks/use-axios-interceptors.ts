import axios, { AxiosError } from 'axios';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useNotification } from '~/contexts/notification-context';
import { ApiErrorResponse } from '~/types';

function useAxiosInterceptors() {
	const { t } = useTranslation('common');

	const { pushNotification } = useNotification();

	useEffect(() => {
		const interceptor = axios.interceptors.response.use(
			(res) => res,
			(err: AxiosError<ApiErrorResponse>) => {
				if (err.code === 'ERR_NETWORK') {
					pushNotification({
						message: t('seemToBeOffline'),
						severity: 'warning',
					});
				}

				throw err;
			},
		);

		return () => {
			axios.interceptors.response.eject(interceptor);
		};
	}, [pushNotification, t]);
}

export default useAxiosInterceptors;
