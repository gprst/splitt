import { App } from '@capacitor/app';
import { Capacitor, PluginListenerHandle } from '@capacitor/core';
import { useEffect } from 'react';

function useNativeBackButton(callback: () => void) {
	useEffect(() => {
		let backButtonListener: PluginListenerHandle | undefined;

		if (Capacitor.getPlatform() === 'android') {
			backButtonListener = App.addListener('backButton', callback);
		}

		return () => {
			backButtonListener?.remove();
		};
	}, [callback]);
}

export default useNativeBackButton;
