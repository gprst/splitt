import { useEffect, useState } from 'react';
import { Notification } from '~/contexts/notification-context';

export type NotificationState = Notification & {
	key: number;
	isOpen: boolean;
};

function useNotificationState() {
	const [notificationsStack, setNotificationsStack] = useState<Notification[]>([]);

	const [notificationState, setNotificationState] = useState<NotificationState>({
		key: 0,
		isOpen: false,
		message: '',
		severity: 'info',
	});

	useEffect(() => {
		if (notificationsStack.length > 0) {
			if (!notificationState.message) {
				// Display notification if there isn't an active one already
				setNotificationState({
					...notificationsStack[0],
					key: new Date().getTime(),
					isOpen: true,
				});
				setNotificationsStack((prev) => prev.slice(1));
			} else if (notificationState.message && notificationState.isOpen) {
				// Close active notification when a new one is added
				setNotificationState((prev) => ({ ...prev, isOpen: false }));
			}
		}
	}, [notificationsStack, notificationState]);

	return {
		notificationState,
		setNotificationState,
		setNotificationsStack,
	};
}

export default useNotificationState;
