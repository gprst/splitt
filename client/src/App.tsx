import { Outlet } from 'react-router-dom';
import useAppUrlOpenListener from './hooks/use-app-url-open-listener';
import useAxiosInterceptors from './hooks/use-axios-interceptors';

function App() {
	useAxiosInterceptors();

	useAppUrlOpenListener();

	return <Outlet />;
}

export default App;
