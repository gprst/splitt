import '@fontsource/lobster-two/400.css';
import '@fontsource/roboto-slab/300.css';
import '@fontsource/roboto-slab/400.css';
import '@fontsource/roboto-slab/500.css';
import '@fontsource/roboto-slab/700.css';
import { CssBaseline, ThemeProvider } from '@mui/material';
import axios from 'axios';
import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { createBrowserRouter, Navigate, RouterProvider } from 'react-router-dom';
import App from './App';
import FullPageLoader from './components/FullPageLoader';
import theme from './consts/theme';
import { NotificationProvider } from './contexts/notification-context';
import './i18n';
import './index.css';
import Landing from './pages/Landing';
import ProjectPage from './pages/Project';

axios.defaults.baseURL = import.meta.env.VITE_API_BASE_URL;
axios.defaults.timeout = 3000;

const router = createBrowserRouter([
	{
		path: '/',
		element: <App />,
		children: [
			{
				path: '',
				element: <Landing />,
			},
			{
				path: ':id',
				element: <ProjectPage />,
			},
			{
				path: '*',
				element: <Navigate to="/" />,
			},
		],
	},
]);

createRoot(document.getElementById('root') as HTMLElement).render(
	<StrictMode>
		<CssBaseline />
		<ThemeProvider theme={theme}>
			<NotificationProvider>
				<RouterProvider
					router={router}
					fallbackElement={<FullPageLoader />}
				/>
			</NotificationProvider>
		</ThemeProvider>
	</StrictMode>,
);
