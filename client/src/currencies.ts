export const currencyCodes = ['EUR', 'USD'] as const;

export type CurrencyCode = typeof currencyCodes[number];

type CurrencyMeta = {
	fraction: number;
	symbol: string;
};

export const currencies: Record<CurrencyCode, CurrencyMeta> = {
	EUR: { fraction: 2, symbol: '€' },
	USD: { fraction: 2, symbol: '$' },
};
