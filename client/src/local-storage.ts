import { ProjectData } from './types';

type LocalStorage = {
	projects: ProjectData[];
};

const localStorage = {
	get<
		Key extends keyof LocalStorage,
		Value extends LocalStorage[Key]
	>(key: Key): Value | null {
		const item = window.localStorage.getItem(key);
		try {
			return item && JSON.parse(item);
		} catch {
			return item as unknown as Value;
		}
	},
	set<
		Key extends keyof LocalStorage,
		Value extends LocalStorage[Key]
	>(key: Key, value: Value): void {
		window.localStorage.setItem(
			key,
			typeof value === 'object' ? JSON.stringify(value) : value,
		);
	}
};

export default localStorage;
