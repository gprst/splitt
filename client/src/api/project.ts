import axios, { AxiosResponse } from 'axios';
import { Project } from '~/types';

export async function postProject(project: Omit<Project, 'id'>): Promise<string> {
	const res = await axios.post('/projects', project);
	const id = res.headers.location?.split('/').pop() as string;
	return id;
}

export async function getProject(projectId: string): Promise<AxiosResponse<Project>> {
	return axios.get<Project>(`/projects/${projectId}`);
}

export async function putProject(updatedProject: Project) {
	const { id, ...project } = updatedProject;
	await axios.put(`/projects/${id}`, project);
}
