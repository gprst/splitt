import axios from 'axios';
import { Expense } from '~/types';

export async function getExpenses(projectId: string): Promise<Expense[]> {
	return (await axios.get<Expense[]>(`/projects/${projectId}/expenses`)).data;
}

export async function postExpense(
	projectId: string,
	expense: Omit<Expense, 'id'>,
): Promise<string | undefined> {
	const res = await axios.post(`/projects/${projectId}/expenses`, expense);
	const id = res.headers.location?.split('/').pop();
	return id;
}

export async function putExpense(projectId: string, updatedExpense: Expense) {
	const { id: expenseId, ...expense } = updatedExpense;
	await axios.put(`/projects/${projectId}/expenses/${expenseId}`, expense);
}

export async function deleteExpense(projectId: string, expenseId: string) {
	await axios.delete(`/projects/${projectId}/expenses/${expenseId}`);
}
