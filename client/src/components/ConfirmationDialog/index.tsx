import { Button, ButtonProps, Dialog, DialogActions, DialogTitle } from '@mui/material';
import { useTranslation } from 'react-i18next';

type Props = {
	confirm: () => void | Promise<void>;
	confirmationColor?: ButtonProps['color'];
	isOpen: boolean;
	message: string;
	onClose: () => void;
};

function ConfirmationDialog({
	isOpen,
	confirm,
	confirmationColor,
	message,
	onClose,
}: Props) {
	const { t } = useTranslation('common');

	return (
		<Dialog
			open={isOpen}
			onClose={onClose}
		>
			<DialogTitle sx={{ whiteSpace: 'pre-line' }}>
				{message}
			</DialogTitle>

			<DialogActions>
				<Button onClick={onClose}>
					{t('cancel')}
				</Button>

				<Button
					color={confirmationColor ?? 'primary'}
					onClick={async () => {
						await confirm();
						onClose();
					}}
				>
					{t('delete')}
				</Button>
			</DialogActions>
		</Dialog>
	);
}

export default ConfirmationDialog;
