import { Alert, Snackbar } from '@mui/material';
import { Dispatch, SetStateAction } from 'react';
import { NotificationState } from '~/hooks/use-notification-state';
import capitalizeFirstLetter from '~/utils/capitalize-first-letter';

type Props = {
	notificationState: NotificationState;
	setNotificationState: Dispatch<SetStateAction<NotificationState>>;
}

function NotificationsWrapper({
	notificationState,
	setNotificationState,
}: Props) {
	return (
		<Snackbar
			key={notificationState.key}
			anchorOrigin={{
				vertical: 'bottom',
				horizontal: 'left',
			}}
			open={notificationState.isOpen}
			onClose={(_, reason) => {
				if (reason !== 'clickaway') {
					setNotificationState((prev) => ({ ...prev, isOpen: false }));
				}
			}}
			autoHideDuration={5000}
			TransitionProps={{
				onExited: () => setNotificationState((prev) => ({ ...prev, message: '' })),
			}}
			sx={{ bottom: { xs: 90, sm: 32 } }}
		>
			<Alert
				elevation={4}
				severity={notificationState.severity}
				onClose={() => setNotificationState((prev) => ({ ...prev, isOpen: false }))}
				sx={{ width: '100%' }}
			>
				{capitalizeFirstLetter(notificationState.message)}
			</Alert>
		</Snackbar>
	);
}

export default NotificationsWrapper;
