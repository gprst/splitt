import { CircularProgress } from '@mui/material';

function FullPageLoader() {
	return (
		<CircularProgress
			color="primary"
			sx={{
				position: 'absolute',
				left: 0,
				right: 0,
				top: 0,
				bottom: 0,
				marginLeft: 'auto',
				marginRight: 'auto',
				marginTop: 'auto',
				marginBottom: 'auto',
			}}
		/>
	);
}

export default FullPageLoader;
