import { keyframes, styled } from '@mui/material';
import fabPositionning from '~/consts/fab-positionning';

const FabPulse = styled('div')(({ theme }) => {
	const { right, bottom } = fabPositionning;

	const pulseAnimation = keyframes({
		from: {
			right: right.xs,
			bottom: bottom.xs,
			width: 56,
			height: 56,
			opacity: '100%',
		},
		to: {
			right: right.xs / 2,
			bottom: bottom.xs / 2,
			width: 72,
			height: 72,
			opacity: '0%',
		}
	});

	return {
		position: 'fixed',
		borderRadius: '50%',
		backgroundColor: `${theme.palette.primary.main}`,
		animation: `${pulseAnimation} 1s ease infinite`,
		[theme.breakpoints.up('sm')]: {
			marginRight: right.sm - right.xs,
			marginBottom: bottom.sm - bottom.xs,
		},
		[theme.breakpoints.up('lg')]: {
			marginRight: right.lg - right.xs,
		},
	};
});

export default FabPulse;
