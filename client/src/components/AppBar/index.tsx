import { ArrowBack as ArrowBackIcon } from '@mui/icons-material';
import {
	AppBar as MuiAppBar,
	Box,
	Container,
	IconButton,
	Toolbar,
	Typography,
	useMediaQuery,
	useTheme
} from '@mui/material';
import { ReactNode } from 'react';
import { Link } from 'react-router-dom';
import './index.css';

type Props = {
	projectTitle?: string;
	rightSide?: ReactNode;
};

function AppBar({ projectTitle, rightSide }: Props) {
	const isMobileView = useMediaQuery(useTheme().breakpoints.only('xs'));

	return (
		<MuiAppBar color="secondary" position="sticky">
			<Container disableGutters maxWidth="lg">
				<Toolbar sx={{ display: 'flex', justifyContent: 'space-between' }}>
					<Box
						sx={{
							display: 'flex',
							alignItems: 'center',
							maxWidth: '85%',
						}}
					>
						{projectTitle && isMobileView ? (
							<IconButton
								color='inherit'
								component={Link}
								to="/"
								sx={{ pl: 0 }}
							>
								<ArrowBackIcon />
							</IconButton>
						) : (
							<Typography
								variant={isMobileView ? 'h4' : 'h3'}
								component={Link}
								to="/"
								sx={{
									color: 'inherit',
									fontFamily: 'Lobster Two Brand',
									textDecoration: 'none',
								}}
							>
								Splitt
							</Typography>
						)}

						{projectTitle && (
							<Typography
								className="truncate"
								variant='h6'
								sx={{
									ml: {
										sm: 4,
									},
								}}
							>
								{projectTitle}
							</Typography>
						)}
					</Box>

					<>{rightSide}</>
				</Toolbar>
			</Container>
		</MuiAppBar >
	);
}

export default AppBar;
