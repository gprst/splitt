import { Add as AddIcon } from '@mui/icons-material';
import {
	Autocomplete,
	IconButton,
	InputAdornment,
	TextField
} from '@mui/material';
import { useState } from 'react';
import { Control, Controller } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { Project } from '~/types';

type Props = {
	control: Control<Omit<Project, 'id'>>;
	expenseParticipants: string[] | undefined;
};

function ParticipantField({
	control,
	expenseParticipants,
}: Props) {
	const { t } = useTranslation('common');

	const [inputValue, setInputValue] = useState('');
	const [errorMessage, setErrorMessage] = useState('');

	const trimParticipantNames = (participantNames: string[]) => {
		const trimmedNames: string[] = [];
		for (const participantName of participantNames) {
			const trimmedName = participantName.trim();
			if (!trimmedNames.includes(trimmedName)) {
				trimmedNames.push(trimmedName);
			}
		}
		return trimmedNames;
	};

	return (
		<Controller
			name="participants"
			control={control}
			rules={{ required: t('projectForm.participantsField.noParticipantError') }}
			render={({ field, fieldState: { error } }) => (
				<Autocomplete
					{...field}
					disableClearable
					freeSolo
					multiple
					onChange={(_, participants) => {
						const isExpensePartipantRemoved = !expenseParticipants?.every(
							(participant) => participants.includes(participant),
						);

						if (expenseParticipants && isExpensePartipantRemoved) {
							setErrorMessage(t(
								'projectForm.participantsField.nonRemoveableParticipantError',
							));
						} else {
							field.onChange(trimParticipantNames(participants));
							setInputValue('');
						}
					}}
					options={[]}
					renderInput={(params) => (
						<TextField
							{...params}
							error={Boolean(error || errorMessage)}
							helperText={error?.message ?? errorMessage}
							label="Participants"
							onBlur={() => setErrorMessage('')}
							onChange={(e) => setInputValue(e.target.value)}
							placeholder={t('projectForm.participantsField.placeholder')}
							inputProps={{
								...params.inputProps,
								value: inputValue
							}}
							InputProps={{
								...params.InputProps,
								endAdornment: (
									<InputAdornment position="end">
										<IconButton
											aria-label={t('projectForm.participantsField.addParticipant')}
											onClick={() => {
												const value = inputValue.trim();
												if (value && !field.value.includes(value)) {
													field.onChange((field.value.concat(value)));
												}
												setInputValue('');
											}}
										>
											<AddIcon />
										</IconButton>
									</InputAdornment>
								)
							}}
						/>
					)}
					ChipProps={{ color: 'primary' }}
				/>
			)}
		/>
	);
}

export default ParticipantField;
