import {
	Button,
	DialogActions,
	DialogContent,
	Grid,
	MenuItem,
	TextField
} from '@mui/material';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useNotification } from '~/contexts/notification-context';
import { CurrencyCode, currencyCodes } from '~/currencies';
import { Project } from '~/types';
import DialogWithTitleBar from '../DialogWithTitleBar';
import ParticipantField from './ParticipantField';
import { postProject, putProject } from '~/api/project';
import { useNavigate } from 'react-router-dom';
import localStorage from '~/local-storage';

type Props = {
	isOpen: boolean;
	onClose: () => void;
} & (
	| {
		expenseParticipants: string[];
		project: Project;
	}
	| {
		expenseParticipants?: never
		project?: never;
	}
);

function ProjectFormDialog({
	expenseParticipants,
	isOpen,
	onClose,
	project,
}: Props) {
	const { control, handleSubmit } = useForm<Omit<Project, 'id'>>({
		mode: 'onTouched',
		defaultValues: {
			title: project?.title ?? '',
			currency: project?.currency ?? '' as CurrencyCode,
			description: project?.description ?? '',
			participants: project?.participants ?? [],
		},
		shouldUnregister: true,
	});

	const { t } = useTranslation(['common', 'project']);

	const { pushNotification } = useNotification();

	const navigate = useNavigate();

	const onSubmit: SubmitHandler<Omit<Project, 'id'>> = async (data) => {
		try {
			if (project) {
				await putProject({ ...data, id: project.id });

				pushNotification({
					message: t('project:projectUpdateSuccessful'),
					severity: 'success',
				});

				// Update locally stored projects data
				const locallyStoredProjects = localStorage.get('projects') ?? [];
				const projectIndex = locallyStoredProjects.findIndex(({ id }) => id === project.id);
				console.log({ projectIndex });
				locallyStoredProjects[projectIndex].description = data.description;
				locallyStoredProjects[projectIndex].title = data.title;
				localStorage.set('projects', locallyStoredProjects);
			} else {
				const id = await postProject(data);

				pushNotification({
					message: t('projectCreationSuccessMessage', { projectTitle: data.title }),
					severity: 'success',
				});

				navigate(`${id}`);
			}
		} catch {
			pushNotification({
				message: t(project ? 'error.modification' : 'error.creation'),
				severity: 'error',
			});
		}

		onClose();
	};

	return (
		<DialogWithTitleBar
			open={isOpen}
			onClose={onClose}
			title={
				project
					? t('projectForm.projectSettings')
					: t('projectForm.newProject')
			}
		>
			<form onSubmit={handleSubmit(onSubmit)}>
				<DialogContent>
					<Grid container spacing={2}>
						{/* Title field */}
						<Grid item xs={12} sm={7}>
							<Controller
								name="title"
								control={control}
								rules={{ required: t('required') }}
								render={({ field, fieldState: { error } }) => (
									<TextField
										{...field}
										fullWidth
										error={Boolean(error)}
										helperText={error?.message}
										label={t('projectForm.titleFieldLabel')}
									/>
								)}
							/>
						</Grid>

						{/* Currency field */}
						<Grid item xs={12} sm={5}>
							<Controller
								name="currency"
								control={control}
								rules={{ required: t('required') }}
								render={({ field, fieldState: { error } }) => (
									<TextField
										{...field}
										fullWidth
										select
										error={Boolean(error)}
										helperText={error?.message}
										label={t('projectForm.currencyFieldLabel')}
									>
										{currencyCodes.map((currency) => (
											<MenuItem key={currency} value={currency}>
												{currency}
											</MenuItem>
										))}
									</TextField>
								)}
							/>
						</Grid>

						{/* Description field */}
						<Grid item xs={12}>
							<Controller
								name="description"
								control={control}
								render={({ field }) => (
									<TextField
										{...field}
										fullWidth
										label={t('projectForm.descriptionFieldLabel')}
									/>
								)}
							/>
						</Grid>

						{/* Participants field */}
						<Grid item xs={12}>
							<ParticipantField
								control={control}
								expenseParticipants={expenseParticipants}
							/>
						</Grid>
					</Grid>
				</DialogContent>

				<DialogActions>
					<Button color="inherit" onClick={onClose}>
						{t('cancel')}
					</Button>

					<Button type="submit">
						{t('confirm')}
					</Button>
				</DialogActions>
			</form>
		</DialogWithTitleBar>
	);
}

export default ProjectFormDialog;
