import {
	AppBar,
	Dialog,
	DialogProps,
	Toolbar,
	Typography
} from '@mui/material';
import { PropsWithChildren, ReactNode } from 'react';

type Props = {
	onClose: DialogProps['onClose'];
	open: boolean;
	title: string;
	titleBarRightContent?: ReactNode;
};

function DialogWithTitleBar({
	children,
	onClose,
	open,
	title,
	titleBarRightContent,
}: PropsWithChildren<Props>) {
	return (
		<Dialog
			fullWidth
			maxWidth="xs"
			open={open}
			onClose={onClose}
		>
			<AppBar position="relative">
				<Toolbar
					sx={{
						display: 'flex',
						justifyContent: 'space-between',
					}}
				>
					<Typography variant="h5">
						{title}
					</Typography>

					{titleBarRightContent}
				</Toolbar>
			</AppBar>

			{children}
		</Dialog>
	);
}

export default DialogWithTitleBar;
