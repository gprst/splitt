import { Add as AddIcon } from '@mui/icons-material';
import { Fab as MuiFab, Tooltip } from '@mui/material';
import { createPortal } from 'react-dom';
import fabPositionning from '~/consts/fab-positionning';
import FabPulse from '../FabPulse';

type Props = {
	label: string;
	onClick: () => void;
	pulse?: boolean;
};

function Fab({ label, onClick, pulse }: Props) {
	const body = window.document.querySelector('body') as HTMLBodyElement;

	return createPortal((
		<>
			{pulse && <FabPulse />}

			<Tooltip title={label} placement="left">
				<MuiFab
					aria-label={label}
					color="primary"
					onClick={onClick}
					sx={fabPositionning}
				>
					<AddIcon />
				</MuiFab>
			</Tooltip>
		</>
	), body);
}

export default Fab;
