import {
	DialogContent,
	Typography
} from '@mui/material';
import { useTranslation } from 'react-i18next';
import DialogWithTitleBar from '~/components/DialogWithTitleBar';
import ProjectAccessForm from './ProjectAccessForm';

type Props = {
	isOpen: boolean;
	onClose: () => void;
};

const ProjectAccessFormDialog = ({ isOpen, onClose }: Props) => {
	const { t } = useTranslation('landing');

	return (
		<DialogWithTitleBar
			open={isOpen}
			onClose={onClose}
			title={t('projectAccessDialog.title')}
		>
			<DialogContent sx={{ display: 'flex', flexDirection: 'column', rowGap: 2 }}>
				<Typography>{t('projectAccessDialog.indication')}</Typography>
				<ProjectAccessForm onSubmit={onClose} />
			</DialogContent>
		</DialogWithTitleBar>
	);
};

export default ProjectAccessFormDialog;
