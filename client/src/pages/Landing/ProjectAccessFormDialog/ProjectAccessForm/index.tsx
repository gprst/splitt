import { ArrowForward as ArrowForwardIcon } from '@mui/icons-material';
import { IconButton, InputAdornment, TextField } from '@mui/material';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';

type FormFields = {
	projectUrl: string;
};

type Props = {
	onSubmit: () => void;
};

function ProjectAccessForm({ onSubmit }: Props) {
	const { t } = useTranslation('landing');

	const {
		formState: { isValid: isFormValid },
		handleSubmit,
		register
	} = useForm<FormFields>();

	const navigate = useNavigate();

	const submit: SubmitHandler<FormFields> = ({ projectUrl }) => {
		onSubmit();
		navigate(projectUrl.split('/').pop() ?? '/');
	};

	const projectUrlPattern = new RegExp(`^${import.meta.env.VITE_BASE_URL}/[a-z0-9]{12}$`);

	return (
		<form onSubmit={handleSubmit(submit)}>
			<TextField
				{...register('projectUrl', {
					pattern: projectUrlPattern,
				})}
				autoFocus
				fullWidth
				label={t('projectAccessDialog.inputLabel')}
				placeholder={t(
					'projectAccessDialog.inputPlaceholder',
					{ baseUrl: import.meta.env.VITE_BASE_URL },
				)}
				InputProps={{
					endAdornment: (
						<InputAdornment position='end'>
							<IconButton
								aria-label={t('projectAccessDialog.ctaAriaLabel')}
								color='primary'
								disabled={!isFormValid}
								type='submit'
							>
								<ArrowForwardIcon />
							</IconButton>
						</InputAdornment>
					)
				}}
			/>
		</form>
	);
}

export default ProjectAccessForm;
