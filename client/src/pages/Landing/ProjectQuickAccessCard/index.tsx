import CloseIcon from '@mui/icons-material/Close';
import { Card, CardActionArea, IconButton, Stack, Typography } from '@mui/material';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import ConfirmationDialog from '~/components/ConfirmationDialog';
import { ProjectData } from '~/types';

type Props = {
	projectData: ProjectData;
	remove: (projectId: string) => void;
};

function ProjectQuickAccessCard({ projectData, remove }: Props) {
	const { t } = useTranslation('landing');

	const [isConfirmationDialogOpen, setIsConfirmationDialogOpen] = useState(false);

	return (
		<>
			<Card
				sx={{
					p: 2,
					minHeight: '5.5em',
					boxShadow: {
						xs: 2,
						sm: 3,
					},
					'&:hover': {
						boxShadow: 6,
					},
					position: 'relative',
					display: 'flex',
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: 'space-between',
				}}
			>
				<CardActionArea
					sx={{
						position: 'absolute',
						top: 0,
						bottom: 0,
						left: 0,
						right: 0,
					}}
					component={Link}
					to={`${projectData.id}`}
				/>

				<Stack sx={{ minWidth: 0 }}>
					<Typography className="truncate" variant="h6">
						{projectData.title}
					</Typography>

					<Typography className="truncate">
						{projectData.description}
					</Typography>
				</Stack>

				<IconButton onClick={() => setIsConfirmationDialogOpen(true)}>
					<CloseIcon />
				</IconButton>
			</Card>

			<ConfirmationDialog
				isOpen={isConfirmationDialogOpen}
				confirm={() => remove(projectData.id)}
				confirmationColor='error'
				message={t('quickAccessRemovalConfirmationMessage', {
					projectTitle: projectData.title,
				})}
				onClose={() => setIsConfirmationDialogOpen(false)}
			/>
		</>
	);
}

export default ProjectQuickAccessCard;
