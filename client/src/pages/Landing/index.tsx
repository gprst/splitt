import { App } from '@capacitor/app';
import {
	Box,
	Container,
	Typography,
	useMediaQuery,
	useTheme
} from '@mui/material';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import useNativeBackButton from '~/hooks/use-native-back-button';
import Layout from '~/Layout';
import localStorage from '~/local-storage';
import LandingSpeedDial from './LandingSpeedDial';
import ProjectQuickAccessCard from './ProjectQuickAccessCard';

function LandingPage() {
	const { t } = useTranslation('landing');

	const [locallyStoredProjects, setLocallyStoredProjects] = useState(
		localStorage.get('projects') ?? [],
	);

	const isMobileView = useMediaQuery(useTheme().breakpoints.only('xs'));

	useNativeBackButton(App.exitApp);

	const removeQuickAccess = (quickAccessId: string) => {
		const newLocallyStoredProjects = locallyStoredProjects.slice();

		newLocallyStoredProjects.splice(
			newLocallyStoredProjects.findIndex(({ id }) => id === quickAccessId),
			1,
		);

		localStorage.set('projects', newLocallyStoredProjects);

		setLocallyStoredProjects(newLocallyStoredProjects);
	};

	return (
		<Layout>
			<Container
				maxWidth="lg"
				sx={{
					display: 'flex',
					flexDirection: 'column',
					gap: 4,
					p: {
						xs: 2,
						sm: 6,
					},
				}}
			>
				{locallyStoredProjects.length === 0 && (
					<>
						<Typography variant="h2">
							{t('catchPhrase')}
						</Typography>

						<Typography sx={{ whiteSpace: 'pre-line' }}>
							{t('introduction')}
						</Typography>
					</>
				)}

				{locallyStoredProjects.length > 0 && (
					<div>
						{!isMobileView && (
							<Typography variant="h6" sx={{ mb: 2 }}>
								{t('quickAccess')}
							</Typography>
						)}

						<Box sx={{
							display: 'grid',
							gridTemplateColumns: 'repeat(auto-fill, minmax(320px, 1fr))',
							gap: 2,
						}}>
							{locallyStoredProjects.map((projectData) => (
								<ProjectQuickAccessCard
									key={projectData.id}
									projectData={projectData}
									remove={removeQuickAccess}
								/>
							))}
						</Box>
					</div>
				)}
			</Container>

			<LandingSpeedDial pulse={locallyStoredProjects.length === 0} />
		</Layout>
	);
}

export default LandingPage;
