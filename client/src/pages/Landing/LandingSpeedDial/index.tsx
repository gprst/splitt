import {
	Create as CreateIcon,
	Login as LoginIcon,
} from '@mui/icons-material';
import { Backdrop, SpeedDial, SpeedDialAction, SpeedDialIcon } from '@mui/material';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import FabPulse from '~/components/FabPulse';
import ProjectFormDialog from '~/components/ProjectFormDialog';
import fabPositionning from '~/consts/fab-positionning';
import ProjectAccessFormDialog from '../ProjectAccessFormDialog';

type Props = {
	pulse?: boolean;
};

function LandingSpeedDial({ pulse }: Props) {
	const { t } = useTranslation('landing');

	const [isOpen, setIsOpen] = useState(false);

	const [isProjectFormDialogOpen, setIsProjectFormDialogOpen] = useState(false);

	const [isProjectAccessDialogOpen, setIsProjectAccessDialogOpen] = useState(false);

	const actions = [
		{
			icon: <CreateIcon />,
			name: t('projectSpeedDial.createProject'),
			callback: () => setIsProjectFormDialogOpen(true),
		},
		{
			icon: <LoginIcon />,
			name: t('projectSpeedDial.joinProject'),
			callback: () => setIsProjectAccessDialogOpen(true),
		},
	];

	return (
		<>
			<Backdrop open={isOpen} />

			{pulse && !isProjectFormDialogOpen && <FabPulse />}

			<SpeedDial
				ariaLabel={t('projectSpeedDial.ariaLabel')}
				icon={<SpeedDialIcon />}
				onClose={() => setIsOpen(false)}
				onOpen={() => setIsOpen(true)}
				open={isOpen}
				sx={fabPositionning}
				transitionDuration={0}
			>
				{actions.map((action) => (
					<SpeedDialAction
						key={action.name}
						icon={action.icon}
						onClick={action.callback}
						sx={{
							'& > span': {
								whiteSpace: 'nowrap',
							},
						}}
						tooltipOpen
						tooltipTitle={action.name}
					/>
				))}
			</SpeedDial>

			<ProjectFormDialog
				isOpen={isProjectFormDialogOpen}
				onClose={() => setIsProjectFormDialogOpen(false)}
			/>

			<ProjectAccessFormDialog
				isOpen={isProjectAccessDialogOpen}
				onClose={() => setIsProjectAccessDialogOpen(false)}
			/>
		</>
	);
}

export default LandingSpeedDial;
