import { Box } from '@mui/material';
import { useParams } from 'react-router-dom';
import Fab from '~/components/Fab';
import FullPageLoader from '~/components/FullPageLoader';
import useNativeBackButton from '~/hooks/use-native-back-button';
import Layout from '~/Layout';
import ProjectContent from './components/ProjectContent';
import ProjectMenu from './components/ProjectMenu';
import { UsernameProvider } from './contexts/username-context';
import useExpenses from './hooks/use-expenses';
import useProject from './hooks/use-project';
import useSettlements from './hooks/use-settlements';

function ProjectPage() {
	const { id: projectId = '' } = useParams();

	const project = useProject(projectId);

	const expenses = useExpenses(projectId);

	const settlements = useSettlements(expenses || [], project?.currency || 'EUR');

	useNativeBackButton(() => {
		window.history.back();
	});

	return (
		<UsernameProvider projectId={projectId}>
			<Layout
				appBar={{
					projectTitle: project?.title,
					rightSide: project ? (
						<ProjectMenu
							expenses={expenses ?? []}
							project={project}
						/>
					) : null
				}}
			>
				<Box
					sx={{
						display: 'flex',
						justifyContent: 'center',
						padding: { sm: 2 },
					}}
				>
					{project === undefined || expenses === undefined ? (
						<>
							<FullPageLoader />

							{/* Dummy FAB to prevent it from flickering when entering the page */}
							<Fab
								label=""
								onClick={() => null}
							/>
						</>
					) : (
						<ProjectContent
							expenses={expenses}
							project={project}
							settlements={settlements}
						/>
					)}
				</Box>
			</Layout>
		</UsernameProvider>
	);
}

export default ProjectPage;
