import {
	Box,
	Card,
	CardActionArea,
	Typography
} from '@mui/material';
import Avatar from 'boring-avatars';
import { useState } from 'react';
import { CurrencyCode } from '~/currencies';
import { Expense } from '~/types';
import formatAmount from '~/utils/format-amount';
import ExpenseFormDialog from '../../ExpenseFormDialog';

type Props = {
	currency: CurrencyCode;
	expense: Expense;
	projectId: string;
	projectParticipants: string[];
};

function ExpenseCard({ expense, currency, projectId, projectParticipants }: Props) {
	const [isExpenseFormDialogOpen, setIsExpenseFormDialogOpen] = useState(false);

	return (
		<>
			<Card sx={{
				boxShadow: {
					xs: 2,
					sm: 3,
				},
				'&:hover': {
					boxShadow: 6,
				},
			}}>
				<CardActionArea
					onClick={() => setIsExpenseFormDialogOpen(true)}
					sx={{
						display: 'flex',
						alignItems: 'center',
						gap: 2,
						padding: 2,
					}}
				>
					<Box sx={{ flexShrink: 0 }}>
						<Avatar
							name={expense.payer}
							variant="marble"
							colors={['#CFD8DC', '#607D8B', '#1A2327', '#FF6333', '#FFC400']}
						/>
					</Box>

					<Typography
						className="truncate"
						variant="h6"
					>
						{expense.title}
					</Typography>

					<Typography sx={{ ml: 'auto' }}>
						{formatAmount(expense.amount, currency)}
					</Typography>
				</CardActionArea>
			</Card>

			<ExpenseFormDialog
				expense={expense}
				isOpen={isExpenseFormDialogOpen}
				onClose={() => setIsExpenseFormDialogOpen(false)}
				currency={currency}
				participants={projectParticipants}
				projectId={projectId}
			/>
		</>
	);
}

export default ExpenseCard;
