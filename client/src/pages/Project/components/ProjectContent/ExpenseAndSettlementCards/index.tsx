import { useMediaQuery, useTheme } from '@mui/material';
import { CurrencyCode } from '~/currencies';
import { Expense, Settlement } from '~/types';
import ExpenseCard from '../ExpenseCard';
import SettlementCard from '../SettlementCard';
import DesktopView from './DesktopView';
import MobileView from './MobileView';

type Props = {
	currency: CurrencyCode;
	expenses: Expense[];
	projectId: string;
	projectParticipants: string[];
	settlements: Settlement[];
};

function ExpenseAndSettlementCards({
	currency,
	expenses,
	projectId,
	projectParticipants,
	settlements,
}: Props) {
	const isMobileView = useMediaQuery(useTheme().breakpoints.only('xs'));

	const expenseCards = expenses.map((expense) => (
		<ExpenseCard
			key={expense.id}
			currency={currency}
			expense={expense}
			projectId={projectId}
			projectParticipants={projectParticipants}
		/>
	));

	const settlementCards = settlements.map((settlement) => (
		<SettlementCard
			key={`${settlement.from}-${settlement.to}-${settlement.amount}`}
			currency={currency}
			settlement={settlement}
		/>
	));

	return isMobileView ? (
		<MobileView
			expenseCards={expenseCards}
			settlementCards={settlementCards}
		/>
	) : (
		<DesktopView
			expenseCards={expenseCards}
			settlementCards={settlementCards}
		/>
	);
}

export default ExpenseAndSettlementCards;
