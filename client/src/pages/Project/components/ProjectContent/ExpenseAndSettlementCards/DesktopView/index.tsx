import { Grid } from '@mui/material';

type Props = {
	expenseCards: JSX.Element[];
	settlementCards: JSX.Element[];
};

function DesktopView({
	expenseCards,
	settlementCards,
}: Props) {
	return (
		<Grid
			container
			spacing={2}
			sx={{ display: 'flex', justifyContent: 'center' }}
			maxWidth="md"
		>
			<Grid
				item
				xs={12}
				sm={settlementCards.length > 0 ? 7 : 8}
				sx={{
					display: 'flex',
					flexDirection: 'column',
					gap: 2,
				}}
			>
				{expenseCards}
			</Grid>

			<Grid
				item
				xs={12}
				sm={5}
				sx={{
					display: 'flex',
					flexDirection: 'column',
					gap: 2,
				}}
			>
				{settlementCards}
			</Grid>
		</Grid>
	);
}

export default DesktopView;
