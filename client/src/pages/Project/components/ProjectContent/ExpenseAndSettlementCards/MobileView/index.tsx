import { Box, Stack, Tab, Tabs } from '@mui/material';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import SwipeableViews from 'react-swipeable-views';

type Props = {
	expenseCards: JSX.Element[];
	settlementCards: JSX.Element[];
};

function MobileView({
	expenseCards,
	settlementCards,
}: Props) {
	const [tabIndex, setTabIndex] = useState(0);

	const { t } = useTranslation('project');

	return (
		<Box sx={{ width: '100%' }}>
			{settlementCards.length === 0 ? (
				// Only display expenses with no tabs if there are no settlements
				<Box sx={{ padding: 2 }}>
					<Stack spacing={2}>
						{expenseCards}
					</Stack>
				</Box>
			) : (
				<>
					<Tabs
						value={tabIndex}
						onChange={(_, newTabIndex: number) => setTabIndex(newTabIndex)}
						indicatorColor="primary"
						textColor="inherit"
						variant="fullWidth"
						aria-label={t('mobileTabs.title')}
					>
						<Tab label={t('mobileTabs.expensesTab')} />
						<Tab label={t('mobileTabs.settlementsTab')} />
					</Tabs>

					<SwipeableViews
						index={tabIndex}
						onChangeIndex={(newTabIndex: number) => setTabIndex(newTabIndex)}
						// https://github.com/oliviertassinari/react-swipeable-views/issues/599#issuecomment-657601754
						containerStyle={{
							transition: 'transform 0.35s cubic-bezier(0.15, 0.3, 0.25, 1) 0s'
						}}
					>
						<Box sx={{ padding: 2 }}>
							<Stack spacing={1}>
								{expenseCards}
							</Stack>
						</Box>

						<Box sx={{ padding: 2 }}>
							<Stack spacing={1}>
								{settlementCards}
							</Stack>
						</Box>
					</SwipeableViews>
				</>
			)}
		</Box>
	);
}

export default MobileView;
