import { Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';
import Fab from '~/components/Fab';
import { Expense, Project, Settlement } from '~/types';
import ExpenseFormDialog from '../ExpenseFormDialog';
import ExpenseAndSettlementCards from './ExpenseAndSettlementCards';
import { useState } from 'react';
import { useUsername } from '../../contexts/username-context';
import UsernameFormDialog from './UsernameFormDialog';

type Props = {
	expenses: Expense[];
	project: Project;
	settlements: Settlement[];
};

function ProjectContent({
	expenses,
	project,
	settlements,
}: Props) {
	const { t } = useTranslation('project');

	const [isExpenseFormDialogOpen, setIsExpenseFormDialogOpen] = useState(false);

	const { username } = useUsername();

	const [isUsernameFormDialogOpen, setIsUsernameFormDialogOpen] = useState(!username);

	return (
		<>
			{expenses.length === 0 ? (
				<Typography
					variant="h5"
					sx={{
						textAlign: 'center',
						color: 'gray',
						margin: 2,
						whiteSpace: 'pre-line',
					}}
				>
					{t('noExpense')}
				</Typography>
			) : (
				<ExpenseAndSettlementCards
					currency={project.currency}
					expenses={expenses}
					projectId={project.id}
					projectParticipants={project.participants}
					settlements={settlements}
				/>
			)}

			<Fab
				label={t('addAnExpense')}
				onClick={() => setIsExpenseFormDialogOpen(true)}
				pulse={expenses.length === 0 && !isExpenseFormDialogOpen}
			/>

			<ExpenseFormDialog
				currency={project.currency}
				isOpen={isExpenseFormDialogOpen}
				onClose={() => setIsExpenseFormDialogOpen(false)}
				participants={project.participants}
				projectId={project.id}
			/>

			<UsernameFormDialog
				isOpen={isUsernameFormDialogOpen}
				onClose={() => setIsUsernameFormDialogOpen(false)}
				participants={project.participants}
				projectId={project.id}
			/>
		</>
	);
}

export default ProjectContent;
