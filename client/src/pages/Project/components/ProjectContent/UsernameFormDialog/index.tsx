import { Button, DialogActions, DialogContent, MenuItem, TextField } from '@mui/material';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import DialogWithTitleBar from '~/components/DialogWithTitleBar';
import localStorage from '~/local-storage';
import { useUsername } from '~/pages/Project/contexts/username-context';

type FormFields = {
	username: string;
};

type Props = {
	isOpen: boolean;
	onClose: () => void;
	participants: string[];
	projectId: string;
};

function UsernameFormDialog({ isOpen, onClose, participants, projectId }: Props) {
	const { t } = useTranslation(['project', 'common']);

	const { username, setUsername } = useUsername();

	const { control, formState, handleSubmit } = useForm<FormFields>({
		defaultValues: { username },
	});

	const submitUserName: SubmitHandler<FormFields> = ({ username }) => {
		const locallyStoredProjects = localStorage.get('projects') ?? [];
		const projectData = locallyStoredProjects.find(({ id }) => id === projectId);
		if (projectData) {
			projectData.username = username;
			setUsername(username);
			localStorage.set('projects', locallyStoredProjects);
		}

		onClose();
	};

	return (
		<DialogWithTitleBar
			open={isOpen}
			onClose={onClose}
			title={t('userNameDialog.title')}
		>
			<form onSubmit={handleSubmit(submitUserName)}>
				<DialogContent>
					<Controller
						name="username"
						control={control}
						rules={{ required: true }}
						render={({ field }) => (
							<TextField
								{...field}
								fullWidth
								label={t('userNameDialog.inputLabel')}
								select
							>
								{participants.map((participant) => (
									<MenuItem
										key={participant}
										value={participant}
									>
										{participant}
									</MenuItem>
								))}
							</TextField>
						)}
					/>
				</DialogContent>

				<DialogActions>
					<Button onClick={onClose}>
						{t('common:later')}
					</Button>
					<Button type="submit" disabled={!formState.isValid}>
						{t('common:confirm')}
					</Button>
				</DialogActions>
			</form>
		</DialogWithTitleBar>
	);
}

export default UsernameFormDialog;
