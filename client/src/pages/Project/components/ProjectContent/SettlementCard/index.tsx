import { ArrowForward as ArrowForwardIcon } from '@mui/icons-material';
import { Box, Card, Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { CurrencyCode } from '~/currencies';
import { useUsername } from '~/pages/Project/contexts/username-context';
import { Settlement } from '~/types';
import formatAmount from '~/utils/format-amount';

type Props = {
	currency?: CurrencyCode;
	settlement: Settlement;
};

function SettlementCard({ currency, settlement }: Props) {
	const { t } = useTranslation('common');

	const { username } = useUsername();

	return (
		<Card
			square
			sx={{ padding: 2 }}
		>
			<Box
				sx={{
					display: 'flex',
					alignItems: 'center',
					gap: .5,
				}}
			>
				<Typography
					className="truncate"
					component="span"
					sx={{ fontWeight: username === settlement.from ? 'bold' : 'inherit' }}
					variant="h6"
				>
					{settlement.from + (username === settlement.from ? ` (${t('me')})` : '')}
				</Typography>

				<ArrowForwardIcon />

				<Typography
					className="truncate"
					component="span"
					sx={{ fontWeight: username === settlement.to ? 'bold' : 'inherit' }}
					variant="h6"
				>
					{settlement.to + (username === settlement.to ? ` (${t('me')})` : '')}
				</Typography>
			</Box>

			<Typography>
				{formatAmount(settlement.amount, currency)}
			</Typography>
		</Card>
	);
}

export default SettlementCard;
