import { Share } from '@capacitor/share';
import { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

function useShareProject(projectTitle: string) {
	const [canShareProject, setCanShareProject] = useState(false);

	const { t } = useTranslation('project');

	useEffect(() => {
		Share.canShare().then(({ value }) => {
			setCanShareProject(value);
		});
	}, []);

	const shareProject = useCallback(async () => {
		const projectUrl = import.meta.env.VITE_BASE_URL + window.location.pathname;

		await Share.share({
			dialogTitle: t('projectMenu.shareProject.nativeDialog.dialogTitle', {
				projectTitle,
			}),
			title: t('projectMenu.shareProject.nativeDialog.title', {
				projectTitle,
			}),
			text: t('projectMenu.shareProject.nativeDialog.text', {
				projectTitle
			}),
			url: projectUrl,
		});
	}, [projectTitle, t]);

	return {
		canShareProject,
		shareProject,
	};
}

export default useShareProject;
