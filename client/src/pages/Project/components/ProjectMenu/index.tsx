import {
	MoreVert as MoreVertIcon,
	Person as PersonIcon,
	Send as SendIcon,
	Settings as SettingsIcon
} from '@mui/icons-material';
import {
	IconButton,
	ListItemIcon,
	ListItemText,
	Menu,
	MenuItem,
	SvgIconTypeMap
} from '@mui/material';
import { OverridableComponent } from '@mui/material/OverridableComponent';
import { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ProjectFormDialog from '~/components/ProjectFormDialog';
import { Expense, Project } from '~/types';
import UsernameFormDialog from '../ProjectContent/UsernameFormDialog';
import ShareProjectDialog from './ShareProjectDialog';
import useShareProject from './useShareProject';

type Props = {
	expenses: Expense[];
	project: Project;
};

type MenuItem = {
	label: string;
	icon: OverridableComponent<SvgIconTypeMap<Record<string, never>, 'svg'>> & {
		muiName: string;
	};
	onClick: () => void;
};

const BUTTON_ID = 'project-menu-button';
const MENU_ID = 'project-menu';

function ProjectMenu({
	expenses,
	project,
}: Props) {
	const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

	const [isShareProjectDialogOpen, setIsShareProjectDialogOpen] = useState(false);
	const [isUsernameFormDialogOpen, setIsUsernameFormDialogOpen] = useState(false);
	const [isProjectFormDialogOpen, setIsProjectFormDialogOpen] = useState(false);

	const { t } = useTranslation('project');

	const allExpenseParticipants = useMemo(() => expenses.reduce<string[]>(
		(allParticipants, expense) => {
			if (!allParticipants.includes(expense.payer)) {
				allParticipants.push(expense.payer);
			}

			expense.participants.forEach((participant) => {
				if (!allParticipants.includes(participant)) {
					allParticipants.push(participant);
				}
			});

			return allParticipants;
		},
		[],
	), [expenses]);

	const { canShareProject, shareProject } = useShareProject(project.title);

	const closeMenu = () => setAnchorEl(null);

	const isOpen = Boolean(anchorEl);

	const menuItems: MenuItem[] = [
		{
			label: t('projectMenu.shareProject.label'),
			icon: SendIcon,
			onClick: canShareProject ? shareProject : (() => setIsShareProjectDialogOpen(true)),
		},
		{
			label: t('projectMenu.username'),
			icon: PersonIcon,
			onClick: () => setIsUsernameFormDialogOpen(true),
		},
		{
			label: t('projectMenu.projectSettings'),
			icon: SettingsIcon,
			onClick: () => setIsProjectFormDialogOpen(true),
		},
	];

	return (
		<>
			<IconButton
				id={BUTTON_ID}
				aria-controls={isOpen ? MENU_ID : undefined}
				aria-expanded={isOpen ? 'true' : undefined}
				aria-haspopup="true"
				aria-label={t('projectMenu.ariaLabel')}
				color="inherit"
				onClick={(e) => setAnchorEl(e.currentTarget)}
			>
				<MoreVertIcon />
			</IconButton>

			<Menu
				id={MENU_ID}
				aria-labelledby={BUTTON_ID}
				anchorEl={anchorEl}
				open={isOpen}
				onClick={closeMenu}
				onClose={closeMenu}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
			>
				{menuItems.map(({ label, icon: Icon, onClick }) => (
					<MenuItem
						key={label}
						onClick={onClick}
					>
						<ListItemIcon>
							<Icon />
						</ListItemIcon>

						<ListItemText>
							{label}
						</ListItemText>
					</MenuItem>
				))}
			</Menu>

			<ShareProjectDialog
				isOpen={isShareProjectDialogOpen}
				onClose={() => setIsShareProjectDialogOpen(false)}
				projectTitle={project.title}
			/>

			<UsernameFormDialog
				isOpen={isUsernameFormDialogOpen}
				onClose={() => setIsUsernameFormDialogOpen(false)}
				participants={project.participants}
				projectId={project.id}
			/>

			<ProjectFormDialog
				isOpen={isProjectFormDialogOpen}
				onClose={() => setIsProjectFormDialogOpen(false)}
				expenseParticipants={allExpenseParticipants}
				project={project}
			/>
		</>
	);
}

export default ProjectMenu;
