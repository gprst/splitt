import { ContentCopy as ContentCopyIcon } from '@mui/icons-material';
import {
	Button,
	DialogActions,
	DialogContent,
	Tooltip,
	Typography
} from '@mui/material';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import DialogWithTitleBar from '~/components/DialogWithTitleBar';
import ProjectLinkCopyButton from './ProjectLinkCopyButton';

type Props = {
	isOpen: boolean;
	onClose: () => void;
	projectTitle: string;
};

function ShareProjectDialog({ isOpen, onClose, projectTitle }: Props) {
	const { t } = useTranslation('project');

	const [isTextCopiedTooltipOpen, setIsTextCopiedTooltipOpen] = useState(false);

	const projectUrl = import.meta.env.VITE_BASE_URL + window.location.pathname;

	return (
		<DialogWithTitleBar
			open={isOpen}
			onClose={onClose}
			title={t('projectMenu.shareProject.label')}
		>
			<DialogContent
				sx={{
					display: 'flex',
					flexDirection: 'column',
					gap: 2,
				}}>
				<Typography>
					{t('projectMenu.shareProject.dialog.content', { projectTitle })}
				</Typography>

				<Tooltip
					open={isTextCopiedTooltipOpen}
					onClose={() => setIsTextCopiedTooltipOpen(false)}
					title={t('projectMenu.shareProject.dialog.successTooltip')}
					disableFocusListener
					disableHoverListener
					disableTouchListener
				>
					<ProjectLinkCopyButton
						onClick={async () => {
							await window.navigator.clipboard.writeText(projectUrl);
							setIsTextCopiedTooltipOpen(true);
							setTimeout(() => setIsTextCopiedTooltipOpen(false), 3000);
						}}
					>
						<Typography
							className="truncate"
							component="span"
						>
							{projectUrl}
						</Typography>

						<ContentCopyIcon fontSize="small" />
					</ProjectLinkCopyButton>
				</Tooltip>
			</DialogContent>
			<DialogActions>
				<Button onClick={onClose}>OK</Button>
			</DialogActions>
		</DialogWithTitleBar>
	);
}

export default ShareProjectDialog;
