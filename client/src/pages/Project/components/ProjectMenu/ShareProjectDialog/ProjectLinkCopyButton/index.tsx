import { styled } from '@mui/material';

const ProjectLinkCopyButton = styled('button')(({ theme }) => ({
	alignItems: 'center',
	backgroundColor: 'transparent',
	border: 'none',
	cursor: 'pointer',
	display: 'flex',
	fontFamily: theme.typography.fontFamily,
	fontSize: theme.typography.htmlFontSize,
	gap: 4,
	padding: 0,
	textAlign: 'start',
	textDecoration: 'underline',
}));

export default ProjectLinkCopyButton;
