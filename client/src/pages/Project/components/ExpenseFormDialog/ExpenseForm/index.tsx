import {
	Box,
	Chip,
	Grid,
	InputAdornment,
	MenuItem,
	TextField,
	Typography,
} from '@mui/material';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs, { Dayjs } from 'dayjs';
import { useState } from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { postExpense, putExpense } from '~/api/expense';
import { useNotification } from '~/contexts/notification-context';
import { currencies, CurrencyCode } from '~/currencies';
import { Expense } from '~/types';

type FormFields = {
	title: string;
	amount: string;
	payer: string;
	date: Dayjs;
};

type Props = {
	currency: CurrencyCode;
	expense?: Expense;
	formId: string;
	onSubmit: () => void;
	projectId: string;
	projectParticipants: string[];
};

function ExpenseForm({
	currency,
	expense,
	formId,
	onSubmit,
	projectId,
	projectParticipants
}: Props) {
	const { t } = useTranslation(['project', 'common']);

	const [expenseParticipants, setExpenseParticipants] = useState<string[]>(
		expense?.participants ?? projectParticipants,
	);

	const { control, handleSubmit } = useForm<FormFields>({
		mode: 'onTouched',
		defaultValues: {
			amount: String(expense?.amount ?? ''),
			date: dayjs(expense?.date),
			payer: expense?.payer ?? '',
			title: expense?.title ?? '',
		},
		shouldUnregister: true,
	});

	const { pushNotification } = useNotification();

	const updateExpense = async (expense: Expense) => {
		await putExpense(projectId, expense);

		pushNotification({
			message: t('expenseForm.editionSuccess'),
			severity: 'success',
		});
	};

	const submitNewExpense = async (expense: Omit<Expense, 'id'>) => {
		await postExpense(projectId, expense);

		pushNotification({
			message: t('expenseForm.creationSuccess'),
			severity: 'success',
		});
	};

	// useEffect(() => {
	// 	setExpenseParticipants(expense?.participants ?? participants);
	// }, [expense, participants]);

	const submitForm: SubmitHandler<FormFields> = async (data) => {
		const expenseToSubmit = {
			title: data.title,
			amount: Number(data.amount),
			payer: data.payer,
			date: (data.date as Dayjs).toISOString(),
			participants: expenseParticipants,
		};

		if (expense === undefined) {
			submitNewExpense(expenseToSubmit);
		} else {
			updateExpense({
				id: expense.id,
				...expenseToSubmit,
			});
		}

		onSubmit();
	};

	return (
		<form id={formId} onSubmit={handleSubmit(submitForm)}>
			<Grid container spacing={2}>
				{/* Title field */}
				<Grid item xs={12} sm={7}>
					<Controller
						name='title'
						control={control}
						rules={{ required: t('common:required') }}
						render={({
							field,
							fieldState: { error }
						}) => (
							<TextField
								{...field}
								fullWidth
								error={Boolean(error)}
								helperText={error?.message}
								label={t('project:expenseForm.titleFieldLabel')}
							/>
						)}
					/>
				</Grid>

				{/* Amount field */}
				<Grid item xs={12} sm={5}>
					<Controller
						name='amount'
						control={control}
						rules={{
							validate: (value) => Number(value) > 0
								|| (t('project:expenseForm.amountFieldError') ?? ''),
						}}
						render={({
							field,
							fieldState: { error }
						}) => (
							<TextField
								{...field}
								fullWidth
								error={Boolean(error)}
								helperText={error?.message}
								label={t('project:expenseForm.amountFieldLabel')}
								type='number'
								InputProps={{
									startAdornment: (
										<InputAdornment position='start'>
											{currencies[currency].symbol}
										</InputAdornment>
									),
								}}
							/>
						)}
					/>
				</Grid>

				{/* Payer field */}
				<Grid item xs={12} sm={7}>
					<Controller
						name='payer'
						control={control}
						rules={{ required: t('common:required') }}
						render={({
							field,
							fieldState: { error }
						}) => (
							<TextField
								{...field}
								fullWidth
								select
								error={Boolean(error)}
								helperText={error?.message}
								label={t('project:expenseForm.payerFieldLabel')}
							>
								{projectParticipants.map((participant) => (
									<MenuItem key={participant} value={participant}>
										{participant}
									</MenuItem>
								))}
							</TextField>
						)}
					/>
				</Grid>

				{/* Date field */}
				<Grid item xs={12} sm={5}>
					<Controller
						name='date'
						control={control}
						rules={{ required: t('common:required') }}
						render={({
							field,
							fieldState: { error }
						}) => (
							<LocalizationProvider dateAdapter={AdapterDayjs}>
								<DatePicker
									{...field}
									label={t('project:expenseForm.dateFieldLabel')}
									renderInput={(params) => (
										<TextField
											{...params}
											fullWidth
											error={Boolean(error)}
											helperText={error?.message}
										/>
									)}
								/>
							</LocalizationProvider>
						)}
					/>
				</Grid>

				{/* Participants */}
				<Grid
					item
					xs={12}
					sx={{
						display: 'flex',
						flexDirection: 'column',
						gap: 1,
					}}
				>
					<Typography>
						{t('project:expenseForm.participantsFieldLabel')}
					</Typography>

					<Box sx={{ display: 'flex', gap: 1, flexWrap: 'wrap' }}>
						{projectParticipants.map((participant) => (
							<Chip
								key={participant}
								color={expenseParticipants.includes(participant) ? 'primary' : 'default'}
								label={participant}
								onClick={() => {
									const newExpenseParticipants = [...expenseParticipants];

									if (expenseParticipants.includes(participant)) {
										if (expenseParticipants.length > 1) {
											newExpenseParticipants.splice(
												newExpenseParticipants.indexOf(participant),
												1,
											);
										}
									} else {
										newExpenseParticipants.push(participant);
									}

									setExpenseParticipants(newExpenseParticipants);
								}}
							/>
						))}
					</Box>
				</Grid>
			</Grid>
		</form>
	);
}

export default ExpenseForm;
