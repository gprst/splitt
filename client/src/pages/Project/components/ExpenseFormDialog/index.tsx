import { Delete as DeleteIcon } from '@mui/icons-material';
import { Button, DialogActions, DialogContent, IconButton } from '@mui/material';
import {  useId, useState } from 'react';
import { useTranslation } from 'react-i18next';
import DialogWithTitleBar from '~/components/DialogWithTitleBar';
import { CurrencyCode } from '~/currencies';
import { Expense } from '~/types';
import ConfirmationDialog from '~/components/ConfirmationDialog';
import { deleteExpense } from '~/api/expense';
import { useNotification } from '~/contexts/notification-context';
import ExpenseForm from './ExpenseForm';

type Props = {
	currency: CurrencyCode;
	expense?: Expense
	isOpen: boolean;
	onClose: () => void;
	participants: string[];
	projectId: string;
};

function ExpenseFormDialog({
	currency,
	expense,
	isOpen,
	onClose,
	participants,
	projectId,
}: Props) {
	const { t } = useTranslation(['project', 'common']);

	const [isConfirmationDialogOpen, setIsConfirmationDialogOpen] = useState(false);

	const { pushNotification } = useNotification();

	const formId = useId();

	return (
		<>
			<DialogWithTitleBar
				open={isOpen}
				onClose={onClose}
				title={
					expense
						? t('project:expenseForm.editExpense')
						: t('project:expenseForm.newExpense')
				}
				titleBarRightContent={expense !== undefined && (
					<IconButton
						aria-label={t('project:expenseForm.deleteExpense')}
						color="inherit"
						onClick={() => {
							onClose();
							setIsConfirmationDialogOpen(true);
						}}
					>
						<DeleteIcon />
					</IconButton>
				)}
			>
				<DialogContent>
					<ExpenseForm
						currency={currency}
						expense={expense}
						formId={formId}
						onSubmit={onClose}
						projectId={projectId}
						projectParticipants={participants}
					/>
				</DialogContent>

				<DialogActions>
					<Button color='inherit' onClick={onClose}>
						{t('common:cancel')}
					</Button>

					<Button form={formId} type='submit'>
						{t('common:confirm')}
					</Button>
				</DialogActions>
			</DialogWithTitleBar>

			{expense && (
				<ConfirmationDialog
					confirmationColor='error'
					isOpen={isConfirmationDialogOpen}
					message={t('confirmExpenseDeletion')}
					onClose={() => setIsConfirmationDialogOpen(false)}
					confirm={async () => {
						await deleteExpense(projectId, expense.id);
						pushNotification({
							message: t('expenseForm.deletionSuccess'),
							severity: 'success',
						});
					}}
				/>
			)}
		</>
	);
}

export default ExpenseFormDialog;
