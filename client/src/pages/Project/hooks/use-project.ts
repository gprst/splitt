import { AxiosError } from 'axios';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { getProject } from '~/api/project';
import { useNotification } from '~/contexts/notification-context';
import localStorage from '~/local-storage';
import { ApiErrorResponse, Project, ProjectData } from '~/types';

// Puts project first in the list of projects stored in `localStorage`
function reorderLocallyStoredProjects(project: Project) {
	const locallyStoredProjects = localStorage.get('projects') || [];

	const projectIndex = locallyStoredProjects.findIndex(({ id }) => id === project.id);

	if (projectIndex > -1) {
		locallyStoredProjects.unshift(
			locallyStoredProjects.splice(projectIndex, 1)[0] as ProjectData,
		);
	} else {
		locallyStoredProjects.unshift({
			id: project.id,
			title: project.title,
			description: project.description,
			username: '',
		});
	}

	localStorage.set('projects', locallyStoredProjects);
}

function useProject(projectId: string) {
	const { t } = useTranslation('project');

	const [project, setProject] = useState<Project>();

	const navigate = useNavigate();

	const { pushNotification } = useNotification();

	// Fetch project data
	useEffect(() => {
		getProject(projectId)
			.then(({ data: project }) => {
				setProject(project);
				reorderLocallyStoredProjects(project);
			})
			.catch((error: AxiosError<ApiErrorResponse>) => {
				// Remove dead project from localStorage
				if (error.response?.data.statusCode === 404) {
					const locallyStoredProjects = localStorage.get('projects') || [];

					const projectIndex = locallyStoredProjects.findIndex(({ id }) => id === projectId);

					if (projectIndex > -1) {
						locallyStoredProjects.splice(projectIndex, 1);
						localStorage.set('projects', locallyStoredProjects);
					}

					pushNotification({
						message: t('projectNotFoundErrorMessage'),
						severity: 'error',
					});
				}

				navigate('/');
			});
	}, [navigate, projectId, pushNotification, t]);

	// Listen to updates
	useEffect(() => {
		const projectUpdateSseSource = new EventSource(
			`${import.meta.env.VITE_API_BASE_URL}/projects/${projectId}/sse`,
		);

		projectUpdateSseSource.onmessage = ({ data }) => {
			setProject(JSON.parse(data));
		};

		return () => {
			projectUpdateSseSource.close();
		};
	}, [projectId]);

	return project;
}

export default useProject;
