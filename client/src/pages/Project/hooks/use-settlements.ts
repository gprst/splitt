import currency from 'currency.js';
import { useMemo } from 'react';
import { currencies, CurrencyCode } from '~/currencies';
import { Expense, Settlement } from '~/types';

function useSettlements(expenses: Expense[], currencyCode: CurrencyCode) {
	return useMemo(() => {
		const currencyOptions: currency.Options = {
			symbol: currencies[currencyCode].symbol,
			precision: currencies[currencyCode].fraction,
		};

		// Setting the balance of all participants
		const balances = expenses.reduce<Map<string, currency>>(
			(balances, expense) => {
				// Updating the balance of the expense payer
				balances.set(
					expense.payer,
					(balances.get(expense.payer) || currency(0, currencyOptions)).add(expense.amount),
				);

				// Updating the balance of each expense participant
				currency(expense.amount)
					.distribute(expense.participants.length)
					.forEach((share, index) => {
						const participant = expense.participants[index];
						balances.set(
							participant,
							(balances.get(participant) || currency(0, currencyOptions)).subtract(share),
						);
					});

				return balances;
			},
			new Map(),
		);

		const settlements: Settlement[] = [];

		while (balances.size > 1) {
			let [[debtor, greatestDebt], [creditor, greatestCredit]] = balances;

			balances.forEach((amount, participant) => {
				if (amount.value > greatestCredit.value) {
					creditor = participant;
					greatestCredit = amount;
				}
				if (amount.value <= greatestDebt.value) {
					debtor = participant;
					greatestDebt = amount;
				}
			});

			const isDebtGreater = Math.abs(greatestDebt.value) > greatestCredit.value;
			const isCreditGreater = Math.abs(greatestDebt.value) < greatestCredit.value;

			let amount: number;
			if (isDebtGreater) {
				amount = greatestCredit.value;
				balances.set(debtor, greatestDebt.add(greatestCredit));
				balances.delete(creditor);
			} else if (isCreditGreater) {
				amount = Math.abs(greatestDebt.value);
				balances.set(creditor, greatestCredit.add(greatestDebt));
				balances.delete(debtor);
			} else {
				amount = greatestCredit.value;
				balances.delete(creditor);
				balances.delete(debtor);
			}

			if (amount > 0) {
				settlements.push({
					amount,
					from: debtor,
					to: creditor,
				});
			}
		}

		return settlements;
	}, [expenses, currencyCode]);
}

export default useSettlements;
