import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getExpenses } from '~/api/expense';
import { useNotification } from '~/contexts/notification-context';
import { Expense } from '~/types';

function useFetchExpenses(projectId: string) {
	const [expenses, setExpenses] = useState<Expense[]>();

	const navigate = useNavigate();

	const { pushNotification } = useNotification();

	// Fetch project expenses
	useEffect(() => {
		getExpenses(projectId)
			.then((expenses) => {
				setExpenses(expenses);
			})
			.catch(() => {
				navigate('/');
			});
	}, [navigate, projectId, pushNotification]);

	// Listen to updates
	useEffect(() => {
		const expensesUpdateSseSource = new EventSource(
			`${import.meta.env.VITE_API_BASE_URL}/projects/${projectId}/expenses/sse`,
		);

		expensesUpdateSseSource.onmessage = ({ data }) => {
			setExpenses(JSON.parse(data));
		};

		return () => {
			expensesUpdateSseSource.close();
		};
	}, [projectId]);

	return expenses;
}

export default useFetchExpenses;
