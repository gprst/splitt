import { PropsWithChildren, createContext, useContext, useState } from 'react';
import localStorage from '~/local-storage';

type UsernameContextState = {
	username: string;
	setUsername: (username: string) => void;
};

const UsernameContext = createContext<UsernameContextState | undefined>(undefined);

type ProviderProps = {
	projectId: string;
};

export function UsernameProvider({ children, projectId }: PropsWithChildren<ProviderProps>) {
	const projectData = localStorage.get('projects')?.find(({ id }) => id === projectId);

	const [username, setUsername] = useState(projectData?.username ?? '');

	return (
		<UsernameContext.Provider value={{ username, setUsername }}>
			{children}
		</UsernameContext.Provider>
	);
}

export function useUsername() {
	const context = useContext(UsernameContext);

	if (context === undefined) {
		throw new Error('useUsername must be used within a UsernameProvider');
	}

	return context;
}
