import 'i18next';
import common from '../public/locales/en/common.json';
import landing from '../public/locales/en/landing.json';
import project from '../public/locales/en/project.json';

declare module 'i18next' {
	interface CustomTypeOptions {
		ns: 'common' | 'landing' | 'project';
		resources: {
			common: typeof common;
			landing: typeof landing;
			project: typeof project;
		}
	}
}
