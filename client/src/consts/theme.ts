import { createTheme } from '@mui/material';
import { amber, blueGrey, red } from '@mui/material/colors';

export default createTheme({
	palette: {
		primary: {
			main: blueGrey[900],
		},
		secondary: amber,
		error: {
			main: red[500],
		},
	},
	typography: {
		fontFamily: 'Roboto Slab',
		h1: { fontFamily: 'Lobster Two' },
		h2: { fontFamily: 'Lobster Two' },
		h3: { fontFamily: 'Lobster Two' },
		h4: { fontFamily: 'Lobster Two' },
		h5: { fontFamily: 'Lobster Two' },
	},
});
