export default {
	position: 'fixed',
	right: {
		xs: 16,
		sm: 32,
		lg: 64,
	},
	bottom: {
		xs: 16,
		sm: 32,
	},
};
