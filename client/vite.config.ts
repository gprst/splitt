import react from '@vitejs/plugin-react';
import { resolve } from 'path';
import { defineConfig } from 'vite';
import { VitePWA } from 'vite-plugin-pwa';

// https://vitejs.dev/config/
export default defineConfig({
	build: {
		rollupOptions: {
			output: {
				manualChunks: (id) => {
					if (id.includes('node_modules')) {
						if (id.includes('@mui')) {
							return 'vendor-mui';
						}
						return 'vendor';
					}
				},
			},
		},
	},
	plugins: [
		react(),
		VitePWA({
			registerType: 'autoUpdate',
			workbox: {
				globPatterns: [
					'**/!(privacy-policy)/*.{js,css,html,woff,woff2,manifest,svg,png,json,txt}',
					'**/.well-known/*.json'
				],
			},
		}),
	],
	resolve: {
		alias: {
			'~': resolve(__dirname, 'src'),
		},
	},
	server: {
		host: '127.0.0.1',
	},
});
