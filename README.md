# Splitt

Splitt is a mobile and web application for you to track and balance shared expenses between you and your friends.

The backend is powered by [NestJS](https://nestjs.com) and the frontend application with [React](https://reactjs.org). The mobile app is generated from the web frontend with [Capacitor](https://capacitorjs.com).

Although the app is currently in beta, it is fully usable — though not necessarily bug-free. Please fill an issue if you found any.

## License

See [license](LICENSE.txt).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
